'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.group(() => {
  Route.get("/projects", "ProjectController.getProjectList")
  Route.get("/projects/:id", "ProjectController.getProject")
  Route.post("/projects", "ProjectController.createProject")
  Route.post("/departments", "ProjectController.addDepartment")
  Route.get("/departments/:id", "ProjectController.getDepartments")
  Route.put("/project/department/tasks/:id", "ProjectController.insertTask")
}).prefix('api/v1')
