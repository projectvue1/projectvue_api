'use strict'
const randomstring = require("randomstring")
const { MongoClient, ObjectId } = require('mongodb');
const url = "mongodb://admin:password@192.168.10.205:27017";
const client = new MongoClient(url)


class ProjectController {
  async getProject({ response, params }) {
    await client.connect();
    const db = client.db("projects");
    const collection = db.collection("projects");
    const results = await collection.findOne(
      { _id: ObjectId(params.id) },
      { projection: { projectName: 1, streamLead: 1, customer: 1 } }
    );
    return response.json(results)
  }

  async getProjectList({ response }) {
    await client.connect();
    const db = client.db("projects");
    const collection = db.collection("projects");
    const results = await collection.find({}).project({ projectName: 1 }).toArray()
    console.log(results)
    return response.json(results)
  }

  async createProject({ request, response }) {
    let project = { ...request.body, departments: [] }
    await client.connect();
    const db = client.db("projects");
    const collection = db.collection("projects");
    const results = await collection.insertOne(project);
    return response.json(results)
  }

  async getDepartments({ params, response }) {
    await client.connect();
    const db = client.db("projects");
    const collection = db.collection("projects");
    const result = await collection.findOne({ _id: ObjectId(params.id) }, { projection: { "departments": 1, _id: 0 }})
    return response.json(result.departments)
  }

  async addDepartment({ request, response }) {
    await client.connect();
    const db = client.db("projects");
    const collection = db.collection("projects");
    const result = await collection.updateOne({ _id: ObjectId(request.body.projectId) }, {
      $push: {
        departments: {
          name: request.body.department,
          tasks: []
        }
      }
    })
    return response.json(result)
  }

  async insertTask({ request, params, response }) {
    await client.connect();
    const db = client.db("projects");
    const collection = db.collection("projects");
    const result = await collection.updateOne({ _id: ObjectId(params.id), "departments.name": request.body.department }, {
      $push: {
        "departments.$.tasks": {
        taskId: (request.body.department + "_" + randomstring.generate(7)).replace(" ", "_"),
        ...request.body
      }}
    })
    return response.json(result)
  }
}

module.exports = ProjectController
